$('li').css('margin', '10px');
$('li').attr('id', 'uw');

$('#p1 li').click(function() {
  console.log("$(this):" + $(this));
  $(this).fadeOut(1000, function() {
    console.log("fadeout complete!")
  });
});

$('#p2 li').click(function() {
  console.log("$(this):" + $(this));
  $(this).fadeOut(1000, function() {
    $(this).fadeIn();
  });
  console.log("fadeIn complete!")
});

$('#p3 li').click(function() {
  console.log("$(this):" + $(this));
  $(this).fadeTo( "slow" , 0.5, function() {
    console.log("fadeTo complete!")
  });
});

$('#p4 li').click(function() {
  console.log("$(this):" + $(this));
  $(this).fadeToggle("fast", function() {
    console.log("fadeToggle complete!")
  });
});