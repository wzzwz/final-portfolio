// weather update button click
$('button').on('click', function(e) {
  $('ul li').each(function() {
    console.log("this:" + this);
    $(this).remove();
  });
  $.ajax({
    url: "http://api.wunderground.com/api/342f2eb873595425/geolookup/conditions/q/WA/Bothell.json",
    dataType: "jsonp",
    success: function(parsed_json) {
      var city = parsed_json['location']['city'];
      var state = parsed_json['location']['state'];
      var temp_f = parsed_json['current_observation']['temp_f'];
      var rh = parsed_json['current_observation']['relative_humidity'];
      var weather = parsed_json['current_observation']['weather'];
      var wind = parsed_json['current_observation']['wind_string'];
      var iconurl = parsed_json['current_observation']['icon_url'];
     
      
      
      var str = "<li> Location : " + city + ", " + state + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Current temperature is: " + temp_f + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Relative Humidity is: " + rh + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Current weather is: " + weather + "<img src='" + iconurl + "' />" + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      var str = "<li> Current wind is: " + wind + "</li>";
      $('ul').append(str);
      $('ul li:last').attr('class', 'list-group-item');
      
      
      // console.log("Current temperature in " + location + " is: " + temp_f);
    }
  });
});


